from download_novel import *
import sys, os

judul = input("Put the title here: ")
path = input("Put the file path here: ")
pth = separators(path,'\\','\\')
# \ is a special character in Python string literals: it starts an escape sequence.
# therefore To fix the problem, you need to double the backslash
title = separators(judul,' ','-')
page = 0

##############################################################################################################

try:
    soup = access_site('https://www.novelupdates.com/series/' +title+'/')

    print(" ## CALCULATING NUMBERS OF CHAPTER ## ")
    chapter = calculating_chapters(soup,title)
    print(" ## DOWNLOADING " + str(chapter) + " CHAPTER(S) ## ")                

    downloading_process(soup,title,pth)

    print("DONE")
###################################################################################################################################################################

    
except Exception as e:
    exc_type, exc_obj, exc_tb = sys.exc_info()
    fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
    print(exc_type, fname, exc_tb.tb_lineno)
    print(e)
    print("Oops...there is no novel with that title, make sure that the title is right or check manually on https://www.novelupdates.com/")

