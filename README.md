# Novelupdate Download

# Source of the novel: https://www.novelupdates.com/                                        
If there is any question or bug report or suggestion on how to make this program better
Contact: h.ravendy@gmail.com / discord: BBBunny#1043 

# WARNING!!!                                                             
There is a chance where you might not get the part you want               
Such as, there is another link that you need to click to other page or a pop up said that you need at least 18 years old notice or etc        
That's not the fault of the code, rather than it is how the source which novelupdate provide's work.                                               
This code only take the source which novelupdate provided as it is  

# download_novel
##   How to use this:                                                                        
Step 1: Create a folder for your novel                                                    
Step 2: Put your file path on the second input                                              
Step 3: type the novel you want                                                           
Step 3.5: make sure there is no typo there                                                
Step 4: wait and enjoy, your file will be saved in html form

# series_finder

## Existing Novel type

<ul>
<li>Light Novel</li>
<li>Published Novel</li>
<li>Web Novel</li>
</ul>

## Existing tags
<details>
<summary>A</summary>
<ul>
<li>Abandoned Children </li>
<li>Ability Steal </li>
<li>Absent Parents </li>
<li>Abusive Characters </li>
<li>Academy </li>
<li>Accelerated Growth </li>
<li>Acting </li>
<li>Acupuncture </li>
<li>Adapted from Manga </li>
<li>Adapted from Manhua </li>
<li>Adapted to Anime </li>
<li>Adapted to Drama </li>
<li>Adapted to Drama CD </li>
<li>Adapted to Game </li>
<li>Adapted to Manga </li>
<li>Adapted to Manhua </li>
<li>Adapted to Manhwa </li>
<li>Adapted to Movie </li>
<li>Adapted to Visual Novel </li>
<li>Adopted Children </li>
<li>Adopted Protagonist </li>
<li>Adultery </li>
<li>Adventurers </li>
<li>Affair </li>
<li>Age Progression </li>
<li>Age Regression </li>
<li>Aggressive Characters </li>
<li>Alchemy </li>
<li>Aliens </li>
<li>All-Girls School </li>
<li>Alternate World </li>
<li>Amnesia </li>
<li>Amusement Park </li>
<li>Anal </li>
<li>Ancient China </li>
<li>Ancient Times </li>
<li>Androgynous Characters </li>
<li>Androids </li>
<li>Angels </li>
<li>Animal Characteristics </li>
<li>Animal Rearing </li>
<li>Anti-Magic </li>
<li>Anti-social Protagonist </li>
<li>Antihero Protagonist </li>
<li>Antique Shop </li>
<li>Apartment Life </li>
<li>Apathetic Protagonist </li>
<li>Apocalypse </li>
<li>Appearance Changes </li>
<li>Appearance Different from Actual Age </li>
<li>Archery </li>
<li>Aristocracy </li>
<li>Arms Dealers </li>
<li>Army </li>
<li>Army Building </li>
<li>Arranged Marriage </li>
<li>Arrogant Characters </li>
<li>Artifact Crafting </li>
<li>Artifacts </li>
<li>Artificial Intelligence </li>
<li>Artists </li>
<li>Assassinations </li>
<li>Assassins </li>
<li>Astrologers </li>
<li>Autism </li>
<li>Automatons </li>
<li>Average-looking Protagonist </li>
<li>Award-winning Work </li>
<li>Awkward Protagonist </li>
</ul>
</details>
<details>
<summary>B</summary>
<ul>
<li>Bands </li>
<li>Based on a Movie </li>
<li>Based on a Song </li>
<li>Based on a TV Show </li>
<li>Based on a Video Game </li>
<li>Based on a Visual Novel </li>
<li>Based on an Anime </li>
<li>Battle Academy </li>
<li>Battle Competition </li>
<li>BDSM </li>
<li>Beast Companions </li>
<li>Beastkin </li>
<li>Beasts </li>
<li>Beautiful Female Lead </li>
<li>Bestiality </li>
<li>Betrayal </li>
<li>Bickering Couple </li>
<li>Biochip </li>
<li>Biography </li>
<li>Bisexual Protagonist </li>
<li>Black Belly </li>
<li>Blackmail </li>
<li>Blacksmith </li>
<li>Blind Dates </li>
<li>Blind Protagonist </li>
<li>Blood Manipulation </li>
<li>Bloodlines </li>
<li>Body Swap </li>
<li>Body Tempering </li>
<li>Body-double </li>
<li>Bodyguards </li>
<li>Books </li>
<li>Bookworm </li>
<li>Boss-Subordinate Relationship </li>
<li>Brainwashing </li>
<li>Breast Fetish </li>
<li>Broken Engagement </li>
<li>Brother Complex </li>
<li>Brotherhood </li>
<li>Buddhism </li>
<li>Bullying </li>
<li>Business Management </li>
<li>Businessmen </li>
<li>Butlers </li>
</ul>
</details>
<details>
<summary>C</summary>
<ul>
<li>Calm Protagonist </li>
<li>Cannibalism </li>
<li>Card Games </li>
<li>Carefree Protagonist </li>
<li>Caring Protagonist </li>
<li>Caste System </li>
<li>Cautious Protagonist </li>
<li>Celebrities </li>
<li>Character Growth </li>
<li>Charismatic Protagonist </li>
<li>Charming Protagonist </li>
<li>Chat Rooms </li>
<li>Cheats </li>
<li>Chefs </li>
<li>Child Abuse </li>
<li>Child Protagonist </li>
<li>Childcare </li>
<li>Childhood Friends </li>
<li>Childhood Love </li>
<li>Childhood Promise </li>
<li>Childish Protagonist </li>
<li>Chuunibyou </li>
<li>Clan Building </li>
<li>Classic </li>
<li>Clever Protagonist </li>
<li>Clingy Lover </li>
<li>Clones </li>
<li>Clubs </li>
<li>Clumsy Love Interests </li>
<li>Co-Workers </li>
<li>Cohabitation </li>
<li>Cold Love Interests </li>
<li>Cold Protagonist </li>
<li>Collection of Short Stories </li>
<li>College/university </li>
<li>Coma </li>
<li>Comedic Undertone </li>
<li>Coming of Age </li>
<li>Complex Family Relationships </li>
<li>Conditional Power </li>
<li>Confident Protagonist </li>
<li>Confinement </li>
<li>Conflicting Loyalties </li>
<li>Conspiracies </li>
<li>Contracts </li>
<li>Cooking </li>
<li>Corruption </li>
<li>Cosmic Wars </li>
<li>Cosplay </li>
<li>Couple Growth </li>
<li>Court Official </li>
<li>Cousins </li>
<li>Cowardly Protagonist </li>
<li>Crafting </li>
<li>Crime </li>
<li>Criminals </li>
<li>Cross-dressing </li>
<li>Crossover </li>
<li>Cruel Characters </li>
<li>Cryostasis </li>
<li>Cultivation </li>
<li>Cunnilingus </li>
<li>Cunning Protagonist </li>
<li>Curious Protagonist </li>
<li>Curses </li>
<li>Cute Children </li>
<li>Cute Protagonist </li>
<li>Cute Story </li>
</ul>
</details>
<details>
<summary>D</summary>
<ul>
<li>Dancers </li>
<li>Dao Companion </li>
<li>Dao Comprehension </li>
<li>Daoism </li>
<li>Dark </li>
<li>Dead Protagonist </li>
<li>Death </li>
<li>Death of Loved Ones </li>
<li>Debts </li>
<li>Delinquents </li>
<li>Delusions </li>
<li>Demi-Humans </li>
<li>Demon Lord </li>
<li>Demonic Cultivation Technique </li>
<li>Demons </li>
<li>Dense Protagonist </li>
<li>Depictions of Cruelty </li>
<li>Depression </li>
<li>Destiny </li>
<li>Detectives </li>
<li>Determined Protagonist </li>
<li>Devoted Love Interests </li>
<li>Different Social Status </li>
<li>Disabilities </li>
<li>Discrimination </li>
<li>Disfigurement </li>
<li>Dishonest Protagonist </li>
<li>Distrustful Protagonist </li>
<li>Divination </li>
<li>Divine Protection </li>
<li>Divorce </li>
<li>Doctors </li>
<li>Dolls/puppets </li>
<li>Domestic Affairs </li>
<li>Doting Love Interests </li>
<li>Doting Older Siblings </li>
<li>Doting Parents </li>
<li>Dragon Riders </li>
<li>Dragon Slayers </li>
<li>Dragons </li>
<li>Dreams </li>
<li>Drugs </li>
<li>Druids </li>
<li>Dungeon Master </li>
<li>Dungeons </li>
<li>Dwarfs </li>
<li>Dystopia </li>
</ul>
</details>
<details>
<summary>E</summary>
<ul>
<li>e-Sports </li>
<li>Early Romance </li>
<li>Earth Invasion </li>
<li>Easy Going Life </li>
<li>Economics </li>
<li>Editors </li>
<li>Eidetic Memory </li>
<li>Elderly Protagonist </li>
<li>Elemental Magic </li>
<li>Elves </li>
<li>Emotionally Weak Protagonist </li>
<li>Empires </li>
<li>Enemies Become Allies </li>
<li>Enemies Become Lovers </li>
<li>Engagement </li>
<li>Engineer </li>
<li>Enlightenment </li>
<li>Episodic </li>
<li>Eunuch </li>
<li>European Ambience </li>
<li>Evil Gods </li>
<li>Evil Organizations </li>
<li>Evil Protagonist </li>
<li>Evil Religions </li>
<li>Evolution </li>
<li>Exhibitionism </li>
<li>Exorcism </li>
<li>Eye Powers </li>
</ul>
</details>
<details>
<summary>F</summary>
<ul>
<li>Fairies </li>
<li>Fallen Angels </li>
<li>Fallen Nobility </li>
<li>Familial Love </li>
<li>Familiars </li>
<li>Family </li>
<li>Family Business </li>
<li>Family Conflict </li>
<li>Famous Parents </li>
<li>Famous Protagonist </li>
<li>Fanaticism </li>
<li>Fanfiction </li>
<li>Fantasy Creatures </li>
<li>Fantasy World </li>
<li>Farming </li>
<li>Fast Cultivation </li>
<li>Fast Learner </li>
<li>Fat Protagonist </li>
<li>Fat to Fit </li>
<li>Fated Lovers </li>
<li>Fearless Protagonist </li>
<li>Fellatio </li>
<li>Female Master </li>
<li>Female Protagonist </li>
<li>Female to Male </li>
<li>Feng Shui </li>
<li>Firearms </li>
<li>First Love </li>
<li>First-time Intercourse </li>
<li>Flashbacks </li>
<li>Fleet Battles </li>
<li>Folklore </li>
<li>Forced into a Relationship </li>
<li>Forced Living Arrangements </li>
<li>Forced Marriage </li>
<li>Forgetful Protagonist </li>
<li>Former Hero </li>
<li>Fox Spirits </li>
<li>Friends Become Enemies </li>
<li>Friendship </li>
<li>Fujoshi </li>
<li>Futanari </li>
<li>Futuristic Setting </li>
</ul>
</details>
<details>
<summary>G</summary>
<ul>
<li>Galge </li>
<li>Gambling </li>
<li>Game Elements </li>
<li>Game Ranking System </li>
<li>Gamers </li>
<li>Gangs </li>
<li>Gate to Another World </li>
<li>Genderless Protagonist </li>
<li>Generals </li>
<li>Genetic Modifications </li>
<li>Genies </li>
<li>Genius Protagonist </li>
<li>Ghosts </li>
<li>Gladiators </li>
<li>Glasses-wearing Love Interests </li>
<li>Glasses-wearing Protagonist </li>
<li>Goblins </li>
<li>God Protagonist </li>
<li>God-human Relationship </li>
<li>Goddesses </li>
<li>Godly Powers </li>
<li>Gods </li>
<li>Golems </li>
<li>Gore </li>
<li>Grave Keepers </li>
<li>Grinding </li>
<li>Guardian Relationship </li>
<li>Guilds </li>
<li>Gunfighters </li>
</ul>
</details>
<details>
<summary>H</summary>
<ul>
<li>Hackers </li>
<li>Half-human Protagonist </li>
<li>Half-sister </li>
<li>Handjob </li>
<li>Handsome Male Lead </li>
<li>Hard-Working Protagonist </li>
<li>Harem-seeking Protagonist </li>
<li>Harsh Training </li>
<li>Hated Protagonist </li>
<li>Healers </li>
<li>Heartwarming </li>
<li>Heaven </li>
<li>Heavenly Tribulation </li>
<li>Hell </li>
<li>Helpful Protagonist </li>
<li>Herbalist </li>
<li>Heroes </li>
<li>Heterochromia </li>
<li>Hidden Abilities </li>
<li>Hiding True Abilities </li>
<li>Hiding True Identity </li>
<li>Hikikomori </li>
<li>Homunculus </li>
<li>Honest Protagonist </li>
<li>Hospital </li>
<li>Hot-blooded Protagonist </li>
<li>Human Experimentation </li>
<li>Human Weapon </li>
<li>Human-Nonhuman Relationship </li>
<li>Humanoid Protagonist </li>
<li>Hunters </li>
<li>Hypnotism </li>
</ul>
</details>
<details>
<summary>I</summary>
<ul>
<li>Identity Crisis </li>
<li>Imaginary Friend </li>
<li>Immortals </li>
<li>Imperial Harem </li>
<li>Incest </li>
<li>Incubus </li>
<li>Indecisive Protagonist </li>
<li>Industrialization </li>
<li>Inferiority Complex </li>
<li>Inheritance </li>
<li>Inscriptions </li>
<li>Insects </li>
<li>Interconnected Storylines </li>
<li>Interdimensional Travel </li>
<li>Introverted Protagonist </li>
<li>Investigations </li>
<li>Invisibility </li>
</ul>
</details>
<details>
<summary>J</summary>
<ul>
<li>Jack of All Trades </li>
<li>Jealousy </li>
<li>Jiangshi </li>
<li>Jobless Class </li>
<li>JSDF </li>
</ul>
</details>
<details>
<summary>K</summary>
<ul>
<li>Kidnappings </li>
<li>Kind Love Interests </li>
<li>Kingdom Building </li>
<li>Kingdoms </li>
<li>Knights </li>
<li>Kuudere </li>
</ul>
</details>
<details>
<summary>L</summary>
<ul>
<li>Lack of Common Sense </li>
<li>Language Barrier </li>
<li>Late Romance </li>
<li>Lawyers </li>
<li>Lazy Protagonist </li>
<li>Leadership </li>
<li>Legendary Artifacts </li>
<li>Legends </li>
<li>Level System </li>
<li>Library </li>
<li>Limited Lifespan </li>
<li>Living Abroad </li>
<li>Living Alone </li>
<li>Loli </li>
<li>Loneliness </li>
<li>Loner Protagonist </li>
<li>Long Separations </li>
<li>Long-distance Relationship </li>
<li>Lost Civilizations </li>
<li>Lottery </li>
<li>Love at First Sight </li>
<li>Love Interest Falls in Love First </li>
<li>Love Rivals </li>
<li>Love Triangles </li>
<li>Lovers Reunited </li>
<li>Low-key Protagonist </li>
<li>Loyal Subordinates </li>
<li>Lucky Protagonist </li>
</ul>
</details>
<details>
<summary>M</summary>
<ul>
<li>Magic </li>
<li>Magic Beasts </li>
<li>Magic Formations </li>
<li>Magical Girls </li>
<li>Magical Space </li>
<li>Magical Technology </li>
<li>Maids </li>
<li>Male Protagonist </li>
<li>Male to Female </li>
<li>Male Yandere </li>
<li>Management </li>
<li>Mangaka </li>
<li>Manipulative Characters </li>
<li>Manly Gay Couple </li>
<li>Marriage </li>
<li>Marriage of Convenience </li>
<li>Martial Spirits </li>
<li>Masked Character </li>
<li>Masochistic Characters </li>
<li>Master-Disciple Relationship </li>
<li>Master-Servant Relationship </li>
<li>Masturbation </li>
<li>Matriarchy </li>
<li>Mature Protagonist </li>
<li>Medical Knowledge </li>
<li>Medieval </li>
<li>Mercenaries </li>
<li>Merchants </li>
<li>Military </li>
<li>Mind Break </li>
<li>Mind Control </li>
<li>Misandry </li>
<li>Mismatched Couple </li>
<li>Misunderstandings </li>
<li>MMORPG </li>
<li>Mob Protagonist </li>
<li>Models </li>
<li>Modern Day </li>
<li>Modern Knowledge </li>
<li>Money Grubber </li>
<li>Monster Girls </li>
<li>Monster Society </li>
<li>Monster Tamer </li>
<li>Monsters </li>
<li>Movies </li>
<li>Mpreg </li>
<li>Multiple Identities </li>
<li>Multiple Personalities </li>
<li>Multiple POV </li>
<li>Multiple Protagonists </li>
<li>Multiple Realms </li>
<li>Multiple Reincarnated Individuals </li>
<li>Multiple Timelines </li>
<li>Multiple Transported Individuals </li>
<li>Murders </li>
<li>Music </li>
<li>Mutated Creatures </li>
<li>Mutations </li>
<li>Mute Character </li>
<li>Mysterious Family Background </li>
<li>Mysterious Illness </li>
<li>Mysterious Past </li>
<li>Mystery Solving </li>
<li>Mythical Beasts </li>
<li>Mythology </li>
</ul>
</details>
<details>
<summary>N</summary>
<ul>
<li>Naive Protagonist </li>
<li>Narcissistic Protagonist </li>
<li>Nationalism </li>
<li>Near-Death Experience </li>
<li>Necromancer </li>
<li>Neet </li>
<li>Netorare </li>
<li>Netorase </li>
<li>Netori </li>
<li>Nightmares </li>
<li>Ninjas </li>
<li>Nobles </li>
<li>Non-humanoid Protagonist </li>
<li>Non-linear Storytelling </li>
<li>Nudity </li>
<li>Nurses </li>
</ul>
</details>
<details>
<summary>O</summary>
<ul>
<li>Obsessive Love </li>
<li>Office Romance </li>
<li>Older Love Interests </li>
<li>Omegaverse </li>
<li>Oneshot </li>
<li>Online Romance </li>
<li>Onmyouji </li>
<li>Orcs </li>
<li>Organ Transplant </li>
<li>Organized Crime </li>
<li>Orgy </li>
<li>Orphans </li>
<li>Otaku </li>
<li>Otome Game </li>
<li>Outcasts </li>
<li>Outdoor Intercourse </li>
<li>Outer Space </li>
<li>Overpowered Protagonist </li>
<li>Overprotective Siblings </li>
</ul>
</details>
<details>
<summary>P</summary>
<ul>
<li>Pacifist Protagonist </li>
<li>Paizuri </li>
<li>Parallel Worlds </li>
<li>Parasites </li>
<li>Parent Complex </li>
<li>Parody </li>
<li>Part-Time Job </li>
<li>Past Plays a Big Role </li>
<li>Past Trauma </li>
<li>Persistent Love Interests </li>
<li>Personality Changes </li>
<li>Perverted Protagonist </li>
<li>Pets </li>
<li>Pharmacist </li>
<li>Philosophical </li>
<li>Phobias </li>
<li>Phoenixes </li>
<li>Photography </li>
<li>Pill Based Cultivation </li>
<li>Pill Concocting </li>
<li>Pilots </li>
<li>Pirates </li>
<li>Playboys </li>
<li>Playful Protagonist </li>
<li>Poetry </li>
<li>Poisons </li>
<li>Police </li>
<li>Polite Protagonist </li>
<li>Politics </li>
<li>Polyandry </li>
<li>Polygamy </li>
<li>Poor Protagonist </li>
<li>Poor to Rich </li>
<li>Popular Love Interests </li>
<li>Possession </li>
<li>Possessive Characters </li>
<li>Post-apocalyptic </li>
<li>Power Couple </li>
<li>Power Struggle </li>
<li>Pragmatic Protagonist </li>
<li>Precognition </li>
<li>Pregnancy </li>
<li>Pretend Lovers </li>
<li>Previous Life Talent </li>
<li>Priestesses </li>
<li>Priests </li>
<li>Prison </li>
<li>Prisoner of War </li>
<li>Proactive Protagonist </li>
<li>Programmer </li>
<li>Prophecies </li>
<li>Prosthesis </li>
<li>Prostitutes </li>
<li>Protagonist Falls in Love First </li>
<li>Protagonist Loyal to Love Interest </li>
<li>Protagonist Strong from the Start </li>
<li>Protagonist with Multiple Bodies </li>
<li>Psychic Powers </li>
<li>Psychopaths </li>
<li>Puppeteers </li>
</ul>
</details>
<details>
<summary>Q</summary>
<ul>
<li>Quiet Characters </li>
<li>Quirky Characters </li>
</ul>
</details>
<details>
<summary>R</summary>
<ul>
<li>R-15 </li>
<li>R-18 </li>
<li>Race Change </li>
<li>Racism </li>
<li>Rape </li>
<li>Rape Victim Becomes Lover </li>
<li>Rebellion </li>
<li>Reincarnated as a Monster </li>
<li>Reincarnated as an Object </li>
<li>Reincarnated into a Game World </li>
<li>Reincarnated into Another World </li>
<li>Reincarnation </li>
<li>Religions </li>
<li>Reluctant Protagonist </li>
<li>Reporters </li>
<li>Rescue </li>
<li>Restaurant </li>
<li>Resurrection </li>
<li>Returning from Another World </li>
<li>Revenge </li>
<li>Reverse Harem </li>
<li>Reverse Rape </li>
<li>Rich to Poor </li>
<li>Righteous Protagonist </li>
<li>Rivalry </li>
<li>Romantic Subplot </li>
<li>Roommates </li>
<li>Royalty </li>
<li>Ruthless Protagonist</li>
</ul>
</details>
<details>
<summary>S</summary>
<ul>
<li>Sadistic Characters </li>
<li>Saints </li>
<li>Salaryman </li>
<li>Samurai </li>
<li>Saving the World </li>
<li>Scheming </li>
<li>Schizophrenia </li>
<li>Scientists </li>
<li>Sculptors </li>
<li>Sealed Power </li>
<li>Second Chance </li>
<li>Secret Crush </li>
<li>Secret Identity </li>
<li>Secret Organizations </li>
<li>Secret Relationship </li>
<li>Secretive Protagonist </li>
<li>Secrets </li>
<li>Sect Development </li>
<li>Seduction </li>
<li>Seeing Things Other Humans Can't </li>
<li>Selfish Protagonist </li>
<li>Selfless Protagonist </li>
<li>Seme Protagonist </li>
<li>Senpai-Kouhai Relationship </li>
<li>Sentient Objects </li>
<li>Sentimental Protagonist </li>
<li>Serial Killers </li>
<li>Servants </li>
<li>Seven Deadly Sins </li>
<li>Seven Virtues </li>
<li>Sex Friends </li>
<li>Sex Slaves </li>
<li>Sexual Abuse </li>
<li>Sexual Cultivation Technique </li>
<li>Shameless Protagonist </li>
<li>Shapeshifters </li>
<li>Sharing A Body </li>
<li>Sharp-tongued Characters </li>
<li>Shield User </li>
<li>Shikigami </li>
<li>Short Story </li>
<li>Shota </li>
<li>Shoujo-Ai Subplot </li>
<li>Shounen-Ai Subplot </li>
<li>Showbiz </li>
<li>Shy Characters </li>
<li>Sibling Rivalry </li>
<li>Sibling's Care </li>
<li>Siblings </li>
<li>Siblings Not Related by Blood </li>
<li>Sickly Characters </li>
<li>Sign Language </li>
<li>Singers </li>
<li>Single Parent </li>
<li>Sister Complex </li>
<li>Skill Assimilation </li>
<li>Skill Books </li>
<li>Skill Creation </li>
<li>Slave Harem </li>
<li>Slave Protagonist </li>
<li>Slaves </li>
<li>Sleeping </li>
<li>Slow Growth at Start </li>
<li>Slow Romance </li>
<li>Smart Couple </li>
<li>Social Outcasts </li>
<li>Soldiers </li>
<li>Soul Power </li>
<li>Souls </li>
<li>Spatial Manipulation </li>
<li>Spear Wielder </li>
<li>Special Abilities </li>
<li>Spies </li>
<li>Spirit Advisor </li>
<li>Spirit Users </li>
<li>Spirits </li>
<li>Stalkers </li>
<li>Stockholm Syndrome </li>
<li>Stoic Characters </li>
<li>Store Owner </li>
<li>Straight Seme </li>
<li>Straight Uke </li>
<li>Strategic Battles </li>
<li>Strategist </li>
<li>Strength-based Social Hierarchy </li>
<li>Strong Love Interests </li>
<li>Strong to Stronger </li>
<li>Stubborn Protagonist </li>
<li>Student Council </li>
<li>Student-Teacher Relationship </li>
<li>Subtle Romance </li>
<li>Succubus </li>
<li>Sudden Strength Gain </li>
<li>Sudden Wealth </li>
<li>Suicides </li>
<li>Summoned Hero </li>
<li>Summoning Magic </li>
<li>Survival </li>
<li>Survival Game </li>
<li>Sword And Magic </li>
<li>Sword Wielder </li>
<li>System Administrator </li>
</ul>
</details>
<details>
<summary>T</summary>
<ul>
<li>Teachers </li>
<li>Teamwork </li>
<li>Technological Gap </li>
<li>Tentacles </li>
<li>Terminal Illness </li>
<li>Terrorists </li>
<li>Thieves </li>
<li>Threesome </li>
<li>Thriller </li>
<li>Time Loop </li>
<li>Time Manipulation </li>
<li>Time Paradox </li>
<li>Time Skip </li>
<li>Time Travel </li>
<li>Timid Protagonist </li>
<li>Tomboyish Female Lead </li>
<li>Torture </li>
<li>Toys </li>
<li>Tragic Past </li>
<li>Transformation Ability </li>
<li>Transmigration </li>
<li>Transplanted Memories </li>
<li>Transported into a Game World </li>
<li>Transported into Another World </li>
<li>Transported Modern Structure </li>
<li>Trap </li>
<li>Tribal Society </li>
<li>Trickster </li>
<li>Tsundere </li>
<li>Twins </li>
<li>Twisted Personality </li>
</ul>
</details>
<details>
<summary>U</summary>
<ul>
<li>Ugly Protagonist </li>
<li>Ugly to Beautiful </li>
<li>Unconditional Love </li>
<li>Underestimated Protagonist </li>
<li>Unique Cultivation Technique </li>
<li>Unique Weapon User </li>
<li>Unique Weapons </li>
<li>Unlucky Protagonist </li>
<li>Unreliable Narrator </li>
<li>Unrequited Love </li>
</ul>
</details>
<details>
<summary>V</summary>
<ul>
<li>Valkyries </li>
<li>Vampires </li>
<li>Villainess Noble Girls </li>
<li>Virtual Reality </li>
<li>Vocaloid </li>
<li>Voice Actors </li>
<li>Voyeurism </li>
</ul>
</details>
<details>
<summary>W</summary>
<ul>
<li>Waiters </li>
<li>War Records </li>
<li>Wars </li>
<li>Weak Protagonist </li>
<li>Weak to Strong </li>
<li>Wealthy Characters </li>
<li>Werebeasts </li>
<li>Wishes </li>
<li>Witches </li>
<li>Wizards </li>
<li>World Travel </li>
<li>World Tree </li>
<li>Writers </li>
</ul>
</details>
<details>
<summary>Y</summary>
<ul>
<li>Yandere </li>
<li>Youkai </li>
<li>Younger Brothers </li>
<li>Younger Love Interests </li>
<li>Younger Sisters </li>
</ul>
</details>
<details>
<summary>Z</summary>
<ul>
<li>Zombies</li>
</ul>
</detai