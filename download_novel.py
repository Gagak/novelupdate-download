#################################################
#           download_novel ver 1.3              #
#################################################

##############################################################################        
from urllib.request import Request,urlopen
from bs4 import BeautifulSoup

# This function is used for writing the html code we get
# to a html file in folder destination
def download_file(file_link, folder):
    file = access_site("https:"+file_link)
    save_path = folder
    
    print("Saving file:", save_path)

    fp = open(save_path, 'w+',  encoding='utf-8')
    string =file.prettify()
    fp.write(string)

# this function is used for to reformating the file path and the title of the novel
# to something suitable
def separators(string, separator, joins):
    if(separator is ' '): # if the separator is a space
        split = string.split()
    else:
        split = string.split(separator)
        
    if(joins is '-'):
        joined = joins.join(split).lower()
    else:
        joined = joins.join(split)

    return(joined)

# this function is used for accessing the site we want
# and returning a class called beautifulsoup4
def access_site(site):
    req = Request(site, headers={'User-Agent': 'Mozilla/6.0'})
    a = urlopen(req)
    soup = BeautifulSoup(a, 'html.parser')
    return (soup)

# This function is used for to calculate the total page in
# the accessed site, this function will also returning an integer
def calculating_pages(soup):
    page = 0
    test = [el.text for el in soup.find_all("div", class_='digg_pagination')]
    if(len(test)==0):
        page = 1
    elif(len(test[0].split("…")) == 2):
        page = test[0].split("…")[-1].split()[-2]
    else:
        hmm = test[0].split()[0]
        page = hmm[-1]

    return (page)

# This function is used to calculate the chapters we have in
# novelupdates, this function also returns an integer value
def calculating_chapters(soup,title):
    page = calculating_pages(soup)
    number = 0
    for pg in range(int(page)+1):
        soup2 = access_site('https://www.novelupdates.com/series/'+title+'/?pg=' + str(pg))
        possible_links = soup2.find_all('a')
        lk=[]
        for link in possible_links:
            if link.has_attr('href'):
                if("extnu" in link.attrs['href']):
                    number = number + 1
    return (number)

# This function is used for when we want to download it
def downloading_process(soup,title, path):
    total_pages = calculating_pages(soup)
    total_chapters = calculating_chapters(soup,title)
    for pg in range(int(total_pages)+1):
        soup = access_site('https://www.novelupdates.com/series/'+title+'/?pg=' + str(pg))
        possible_links = soup.find_all('a')
        lk=[]
                    
        for link in possible_links:
            if link.has_attr('href'):
                if("extnu" in link.attrs['href']):
                    if(link.text not in lk):
                        lk.append(link.text)
                        download_file(link.attrs['href'], path + '\\ ' + link.text +".html")
                        
##############################################################################################################

